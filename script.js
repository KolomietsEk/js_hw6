//1. Дана строка 'aaa@bbb@ccc'. Замените все @ на ! с помощью глобального поиска и замены.

/*let str = 'aaa@bbb@ccc';
console.log (str.replace(/@/g, '!'));*/


//2. В переменной date лежит дата в формате 2025-12-31. Преобразуйте эту дату в формат 31/12/2025.

/*let str = '2025-12-31';
let arr = str.split('-').reverse();
//console.log(arr)
let newStr = arr[0] + '/' + arr[1] + '/' + arr[2];
console.log(newStr)*/

//3. Дана строка «Я учу javascript!». Вырежете из нее слово «учу» и слово «javascript» тремя разными способами (через substr, substring, slice).

/*let str = 'Я учу javascript!';
console.log(str.substr(2, 3));
console.log(str.substr(6, 10));

console.log(str.substring(2,5));
console.log(str.substring(6, 16));

console.log(str.slice(2,5));
console.log(str.slice(6, 16));*/

//4. Дан массив с элементами 4, 2, 5, 19, 13, 0, 10. Найдите квадратный корень из суммы кубов его элементов. Для решения воспользуйтесь циклом for.

/*let arr = [4, 2, 5, 19, 13, 0, 10]
let sum = 0;
for (i=0; i < arr.length; i++){
    sum += Math.pow(arr[i], 3)
}
console.log(Math.sqrt(sum));*/

//5. Даны переменные a и b. Отнимите от a переменную b и результат присвойте переменной c. Сделайте так, чтобы в любом случае в переменную c записалось положительное значение. Проверьте работу скрипта при a и b, равных соответственно 3 и 5, 6 и 1.

/*let a = 6;
let b = 1;
let c = Math.abs(a - b);
console.log (c);*/

//6. Выведите на экран текущую дату-время в формате 12:59:59 31.12.2014. Для решения этой задачи напишите функцию, которая будет добавлять 0 перед днями и месяцами, которые состоят из одной цифры (из 1.9.2014 сделает 01.09.2014).

/*let date = new Date();
function getZero(num){
	if (num > 0 && num < 10) { 
		return '0' + num;
	} else {
		return num;
	}
}

console.log(date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + ' ' + getZero(date.getDate()) + '.' + getZero(date.getMonth() + 1) + '.' + date.getFullYear());*/

//7. Дана строка 'aa aba abba abbba abca abea'. Напишите регулярку, которая найдет строки aba, abba, abbba по шаблону: буква 'a', буква 'b' любое количество раз, буква 'a'.

/*let str = 'aa aba abba abbba abca abea';
console.log(str.match(/ab+a/g));*/